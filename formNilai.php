<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css">
    <title>Grading Nilai Siswa</title>

    <!-- CSS -->
    <style>
        .mt-24 {
            margin-top: 24px;
        }

        .center {
            display: flex;
            justify-content: center;
        }

        .bold {
            font-weight: bold;
        }

        .white {
            color: white;
        }

        .form-group {
            margin-bottom: 8px;
        }

        .card .card-body .data-list .data-item {
            margin-bottom: 12px;
        }

        .card .card-body .data-list .data-item h6 {
            display: inline;
        }
    </style>

</head>

<body>

    <!-- PHP Syntax -->

    <?php
            // Pengecekan apakah variable submit sudah pernah diklik
            if(isset($_POST['submit'])){
                // Inisialisasi variabel 
                $nama = $_POST['nama'];
                $mapel = $_POST['mapel'];
                $uts = $_POST['grade_uts'];
                $uas = $_POST['grade_uas'];
                $tugas = $_POST['grade_tugas'];                        

                // Inisialisasi variable total nilai
                // UTS 35%
                // UAS 50%
                // Tugas 15%
                $total_grade = ($uts*(35/100)) + ($uas*(50/100)) + ($tugas*(15/100));

                // Kondisi pengecekan total nilai untuk mendapatkan grade
                // Asumsi Tambahan: Jika siswa mendapat grade A atau B maka dinyatakan lulus, selain itu siswa dinyatakan tidak lulus
                if($total_grade >= 90 && $total_grade <= 100) {
                    $grade = "A";
                    $pass = true; //Lulus
                } else if($total_grade > 70 && $total_grade < 90){
                    $grade = "B";
                    $pass = true; //Lulus
                } else if($total_grade > 50 && $total_grade <= 70){
                    $grade = "C";
                    $pass = false; //Tidak Lulus
                } else if($total_grade <= 50){
                    $grade = "D";
                    $pass = false; //Tidak Lulus
                }

            };

        ?>

    <!-- End PHP Syntax -->

    <div class="container">
        <div class="row mt-24 center">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header bg-primary">
                        <h5 class="text-center" style="color:white;">
                            Formulir Nilai Siswa
                        </h5>
                    </div>
                    <div class="card-body">
                        <form class="needs-validation" action="#" method="POST" novalidate>
                            <div class="form-row">
                                <div class="form-group">
                                    <label for="nama">Nama Siswa</label>
                                    <input type="text" name="nama" class="form-control" id="nama"
                                        placeholder="Masukkan nama siswa" required>
                                    <!-- Error Message -->
                                    <div class="invalid-feedback">
                                        Harap masukkan nama siswa
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="mapel">Mata Pelajaran</label>
                                    <select name="mapel" id="mapel" class="form-control">
                                        <option selected disabled>Pilih Mata Pelajaran</option>
                                        <option value="Ilmu Pengetahuan Alam">Ilmu Pengetahuan Alam</option>
                                        <option value="Ilmu Pengetahuan Sosial">Ilmu Pengetahuan Sosial</option>
                                        <option value="Matematika">Matematika</option>
                                        <option value="Bahasa Indonesia">Bahasa Indonesia</option>
                                    </select>
                                    <!-- Error Message -->
                                    <div class="invalid-feedback">
                                        Harap pilih mata pelajaran
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="grade_uts">Nilai UTS</label>
                                    <input onblur="this.value = valueValidation(this, this.value)" max="100"
                                        type="number" class="form-control" name="grade_uts" id="grade_uts"
                                        placeholder="Masukkan nilai UTS" required>
                                    <!-- Error Message -->
                                    <div class="invalid-feedback">
                                        Harap masukkan nilai UTS
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="grade_uas">Nilai UAS</label>
                                    <input onblur="this.value = valueValidation(this, this.value)" max="100" type="number" class="form-control" name="grade_uas" id="grade_uas"
                                        placeholder="Masukkan nilai UAS" required>
                                    <!-- Error Message -->
                                    <div class="invalid-feedback">
                                        Harap masukkan nilai UAS
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="grade_tugas">Nilai Tugas</label>
                                    <input onblur="this.value = valueValidation(this, this.value)" max="100" type="number" class="form-control" name="grade_tugas"
                                        id="grade_tugas" placeholder="Masukkan nilai tugas" required>
                                    <!-- Error Message -->
                                    <div class="invalid-feedback">
                                        Harap masukkan nilai Tugas
                                    </div>
                                </div>
                                <div class="form-group mt-24">
                                    <button type="submit" name="submit" class="btn btn-primary form-control">Kirim Data
                                        Siswa</button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>

            <!-- HASIL INPUTAN FORM -->
            <?php if(isset($_POST['submit'])): ?> <!-- Kondisi ketika user mengklik button submit -->          
            <div class="col-md-6">
                <!-- Kondisi untuk mengecek apakah user lulus -->
                <div class="card <?php echo $pass == true? "border-success":"border-danger"; ?>">
                    <div class="card-header <?php echo $pass == true? "bg-success":"bg-danger"; ?>">
                        <h5 class="text-center white">
                            Data Siswa
                        </h5>
                    </div>
                    <div class="card-body">
                        <div class="data-list">
                            <div class="data-item">
                                <h6>Nama Siswa: </h6>
                                <span><?php echo $nama ?></span> <!-- Cetak nama -->
                            </div>
                            <div class="data-item">
                                <h6>Mata Pelajaran: </h6>
                                <span><?php echo $mapel ?></span> <!-- Cetak mapel -->
                            </div>
                            <div class="data-item">
                                <h6>Nilai UTS: </h6>
                                <span><?php echo $uts ?></span> <!-- Cetak nilai uts -->
                            </div>
                            <div class="data-item">
                                <h6>Nilau UAS: </h6>
                                <span><?php echo $uas ?></span> <!-- Cetak nilai uas -->
                            </div>
                            <div class="data-item">
                                <h6>Nilai Tugas: </h6>
                                <span><?php echo $tugas ?></span> <!-- Cetak nilai tugas -->
                            </div>
                            <hr>
                            <div class="data-item <?php echo $pass == true? "text-success":"text-danger"; ?>">
                                <h6>Nilai Total: </h6>
                                <span class="bold"><?php echo $total_grade ?></span> <!-- Cetak nilai total -->
                            </div>
                            <div class="data-item <?php echo $pass == true? "text-success":"text-danger"; ?>">
                                <h6>Grade: </h6>
                                <span class="bold"><?php echo $grade ?></span> <!-- Cetak grade -->
                            </div>
                        </div>
                    </div>
                    <div class="card-footer bg-transparent ">
                        <!-- Pengecekan apakah user lulus -->
                        <h5 class="center <?php echo $pass == true? "text-success":"text-danger"; ?>">
                            <?php echo $pass == true? "Lulus":"Tidak Lulus"; ?>
                            <!-- Cetak keterangan lulus atau tidak -->
                        </h5>
                    </div>
                </div>
            </div>

            <?php endif ?>
        </div>
    </div>

    <!-- Bootstrap javascript validation -->
    <script>
        // Max Value Validation        
        'use strict'
        function valueValidation(that, value) {            

            let max = parseInt(that.getAttribute('max'));
            let val = parseInt(value);            

            if (val > max) {
                return max;
            } else {
                return val;
            }
        };
        // Example starter JavaScript for disabling form submissions if there are invalid fields
        (function () {
            'use strict';
            window.addEventListener('load', function () {
                // Fetch all the forms we want to apply custom Bootstrap validation styles to
                var forms = document.getElementsByClassName('needs-validation');
                // Loop over them and prevent submission
                var validation = Array.prototype.filter.call(forms, function (form) {
                    form.addEventListener('submit', function (event) {
                        if (form.checkValidity() === false) {
                            event.preventDefault();
                            event.stopPropagation();
                        }
                        form.classList.add('was-validated');
                    }, false);
                });
            }, false);
        })();
    </script>

</body>

</html>